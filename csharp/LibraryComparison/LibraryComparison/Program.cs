﻿using MathNet.Numerics;
using MathNet.Numerics.Data.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Double.Solvers;
using MathNet.Numerics.LinearAlgebra.Solvers;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace LibraryComparison
{
	class Program
	{
		/// <summary>
		/// Il nome del file su cui scrivere.
		/// </summary>
		public static string FILE_NAME;

		public static int MAX_ITERATIONS = 5000;
		public static int MIN_ITERATIONS = 100;
		public static double TOLERANCE = 1e-6;
		public static string[] SOLVERS = { "GpBiCg", "TFQMR", "BiCgStab", "MlkBiCgStab" };
		public static int MAX_EXEC_MINUTES = 3;
		public static double MAX_ERROR = 0.1d;

		static void Main(string[] args)
		{
			// Test combinatorio del programma
			int[] iterations = { 1000, 3000, 5000, 10000 };
			int[] solverIds = { 0, 1, 2, 3 };
			Console.Write("Test name: ");
			string cwd = ".\\" + Console.ReadLine().Trim('\n', '\r', ' ').Replace(' ', '_') + "\\";
			if (!Directory.Exists(cwd))
			{
				Directory.CreateDirectory(cwd);
			}
			Directory.SetCurrentDirectory(cwd);

			foreach (var s in solverIds)
			{
				foreach (var it in iterations)
				{
					MAX_ITERATIONS = it;
					Console.WriteLine("****************************************************************************");
					Console.WriteLine("NEW MAX ITERATIONS: {0}", MAX_ITERATIONS);
					MainTest(args, s);
				}
			}
		}

		static void MainTest(string[] args, int solverId)
		{
			if (args.Length == 0 || !Directory.Exists(args[0]))
			{
				Console.WriteLine("Usage:\nLibraryComparison <working-dir>");
				Console.ReadKey();
				Environment.Exit(1);
			}

			// Impostazione dell'ambiente di lavoro
			string workingDir = args[0];
			string[] files = Directory.GetFiles(workingDir, "*.mtx");
			Telemetry telemetry = new Telemetry();
			SparseMatrix A;
			IIterativeSolver<double> solver;
			string solverName = GetSolverFromUser(out solver, solverId); // chiede all'utente quale solver utilizzare
			FILE_NAME = $"csharp_telemetry_{solverName}.csv";

			foreach (var f in files)
			{
				telemetry = new Telemetry()
				{
					MatrixName = f.Substring(f.LastIndexOf('\\') + 1),
					MethodUsed = solverName
				};

				Console.WriteLine("Parsing matrix: {0}", telemetry.MatrixName);
				A = (SparseMatrix)MatrixMarketReader.ReadMatrix<double>(f);
				Console.WriteLine("Matrix parsed:\n{0} x {1}, non - zero: {2}.", A.RowCount, A.ColumnCount, A.NonZerosCount);
				Console.WriteLine("Trying to solve the system...");
				AnalyzeMatrix(A, ref telemetry, solver);
				Console.WriteLine("Done. Results: ");

				Console.WriteLine(telemetry);

				telemetry.WriteOnFile(FILE_NAME);
				Console.WriteLine();
			}
			File.AppendAllText(FILE_NAME, $"Method Used:;{telemetry.MethodUsed};\nMax Iterations:;{MAX_ITERATIONS};\nTollerance:;{TOLERANCE};Max error:;{MAX_ERROR};\n\n\n");
			Console.WriteLine("Finished. All matrices were analyzed.");
			Console.WriteLine("Results have been wrote on file {0}\nPress any key to exit...\n", FILE_NAME);
			//Console.ReadKey();
		}

		/// <summary>
		/// Risolve il sistema A * x = b, dove b è calcolato
		/// </summary>
		/// <param name="A"></param>
		/// <param name="results"></param>
		/// <returns></returns>
		static void AnalyzeMatrix(SparseMatrix A, ref Telemetry results, IIterativeSolver<double> solver)
		{
			var cancelToken = new System.Threading.CancellationTokenSource();

			results.Rows = A.RowCount;
			results.Cols = A.ColumnCount;
			results.NonZeros = A.NonZerosCount;

			var iterationCountStopCriterion = new IterationCountStopCriterion<double>(MAX_ITERATIONS);
			var residualStopCriterion = new ResidualStopCriterion<double>(TOLERANCE);
			var failureStopCriterion = new FailureStopCriterion<double>();
			var divergeStopCriterion = new DivergenceStopCriterion<double>(0.08d, MIN_ITERATIONS); // default value
			var monitor = new Iterator<double>(iterationCountStopCriterion, residualStopCriterion, failureStopCriterion, divergeStopCriterion);

			Vector<double> ones, b, x = Vector<double>.Build.Dense(A.ColumnCount, 0.0d);
			ones = Vector<double>.Build.Dense(A.ColumnCount, 1.0d);
			b = A * ones;
			try
			{
				// Se un calcolo ci impiega più di tot minuti, annulla l'operazione!
				Task.Factory.StartNew(() => { System.Threading.Thread.Sleep(1000 * 60 * MAX_EXEC_MINUTES); monitor.Cancel(); }
											, cancelToken.Token);
				results.StartTimer();
				results.Status = A.TrySolveIterative(b, x, solver, monitor);
				results.StopTimer();
				if ((results.Status == IterationStatus.Converged || results.Status == IterationStatus.Diverged)
					&& !x.AlmostEqual(ones, MAX_ERROR)) // Questo TEST DEVE ESSERE CALIBRATO BENE! 0.1d sembra il più adatto...
				{
					results.Status = IterationStatus.Diverged; // Il risultato è scorretto!
				}
				results.RelativeError = Distance.Euclidean(x, ones) / ones.L2Norm();
			}
			catch (NonConvergenceException e)
			{
				Console.WriteLine("\nAn error occurred:{0}\nContinuing with another matrix.", e.Message);
				results.Status = IterationStatus.Failure;
			}
			finally
			{
				iterationCountStopCriterion.Reset();
				residualStopCriterion.Reset();
				monitor.Reset();
				cancelToken.Cancel();
			}
		}

		/// <summary>
		/// Chiede all'utente il nome del solver che preferisce utilizzare e lo ritorna sottoforma di stringa.
		/// Inoltre istanzia un oggetto del tipo preferito.
		/// </summary>
		/// <param name="solver">Reference all'interno del quale verrà istanziato il solver.</param>
		/// <returns>Il nome del solver scelto dall'utente.</returns>
		private static string GetSolverFromUser(out IIterativeSolver<double> solver, int overrideSolver = -1)
		{
			int i = 0;
			Console.WriteLine("Choose a solver to use from the following:\n1. GpBiCg\n2. TFQMR\n3. BiCgStab\n4. MlkBiCgStab");
			Console.Write("Which solver shall use? [1/2/3/4]: ");
			try
			{
				i = overrideSolver >= 0 ? overrideSolver : int.Parse(Console.ReadLine()) - 1;
			}
			catch (FormatException)
			{
				Console.WriteLine("Exiting...");
				Environment.Exit(0);
			}

			switch (i)
			{
				case 0:
					solver = new GpBiCg();
					break;
				case 1:
					solver = new TFQMR();
					break;
				case 2:
					solver = new BiCgStab();
					break;
				case 3:
					solver = new MlkBiCgStab();
					break;
				default:
					solver = new GpBiCg(); // RIMUOVERE E INSERIRE ERRORE!!!!
					break;
			}
			Console.WriteLine("Using solver: {0}\n", SOLVERS[i]);
			return SOLVERS[i];
		}

	}

	struct Telemetry
	{

		Stopwatch timeElapsed;

		/// <summary>
		/// Ritorna il tempo trascorso.
		/// </summary>
		public long TimeElapsed { get { return timeElapsed.ElapsedMilliseconds; } }

		public string MatrixName { get; set; }
		public int Rows { get; set; }
		public int Cols { get; set; }
		public int NonZeros { get; set; }
		public int MemoryUsed { get; set; }
		public int Iterations { get; set; }
		public string MethodUsed { get; set; }
		public IterationStatus Status { get; set; }
		public double RelativeError { get; set; }

		/// <summary>
		/// Avvia il timer.
		/// </summary>
		public void StartTimer()
		{
			timeElapsed = Stopwatch.StartNew();
		}

		/// <summary>
		/// Ferma il timer.
		/// </summary>
		public void StopTimer()
		{
			timeElapsed.Stop();
		}

		/// <summary>
		/// Scrive la telemetria con sintassi csv sul file FILE_NAME
		/// </summary>
		/// <param name="path"></param>
		public void WriteOnFile(string path)
		{
			path = @".\" + path;
			if (!File.Exists(path))
			{
				string header = "Nome matrice;N. Righe;N. Colonne;N. non-zero;Tempo calcolo;Stato;Err. Relativo;\n";
				File.WriteAllText(path, header);
			}
			using (StreamWriter sw = File.AppendText(path))
			{
				sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};", MatrixName, Rows, Cols, NonZeros, TimeElapsed, Status, RelativeError);
			}

		}

		public override string ToString()
		{
			string str = String.Empty;
			str += $"Matrix name:\t{MatrixName}\n";
			str += $"Dimensions:\t{Rows} x {Cols}\n";
			str += $"Non zeros:\t{NonZeros}\n";
			str += $"Time elapsed:\t{TimeElapsed} ms\n";
			str += $"Method used:\t{MethodUsed}\n";
			str += $"Status:\t\t{Status}\n";
			str += $"Rel. error:\t{RelativeError * 100} %";
			return str;
		}

	}
}
