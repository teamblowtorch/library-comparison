function [] = comparison()
    MAX_ITERATIONS = 10000;
    MAX_ERROR      = 0.1;
    MAX_TOLERANCE  = 1e-06; % Stessa di C#
    WORKING_DIR    = 'C:\\Users\\Darius\\Desktop\\matrices\\'; % Impostare qui la cartella dove sono le matrici

    cd(WORKING_DIR);
     
    outFile = fopen('.\\results\\matlab_results.csv', 'w');
    memFile = fopen('.\\results\\memory_usage.csv', 'w');
    fprintf(outFile,'Nome file;Tempo di calcolo;Stato;Iterazioni;Metodo;Erorre relativo;\n');
    
    files = dir('*.mat');
    for i=1:length(files)
        
        file = load(files(i).name);
        A = file.Problem.A;
        
        mem = whos('A'); % Calcola memoria usata da A
        
        [~, fileName, ~] = fileparts(files(i).name);
        fileName
             
        %Jacobi
        [timeElapsed, iterations, result, rel_error] = solve_with(@jacobi, A, MAX_ITERATIONS, MAX_ERROR, MAX_TOLERANCE);
        fprintf(outFile,'%s;%f;%s;%f;Jacobi;%f;\n',fileName, timeElapsed, result, iterations,rel_error);        
        
        %Gauss-Seidel
        [timeElapsed, iterations, result, rel_error] = solve_with(@gauss_seidel, A, MAX_ITERATIONS, MAX_ERROR, MAX_TOLERANCE);
        fprintf(outFile,'%s;%f;%s;%f;Gauss-Seidel;%f;\n',fileName, timeElapsed, result, iterations,rel_error);        
        
        % Biconjugate gradients
        [timeElapsed, iterations, result, rel_error] = solve_with(@bicg, A, MAX_ITERATIONS, MAX_ERROR, MAX_TOLERANCE);
        fprintf(outFile,'%s;%f;%s;%f;BiCg;%f;\n',fileName, timeElapsed, result, iterations,rel_error);
      
        
        % Biconjugate gradients stabilized
        [timeElapsed, iterations, result, rel_error] = solve_with(@bicgstab, A, MAX_ITERATIONS, MAX_ERROR, MAX_TOLERANCE);
        fprintf(outFile,'%s;%f;%s;%f;BiCgStab;%f;\n',fileName, timeElapsed, result, iterations,rel_error);
        
                
        % Standard (A\b = mldivide(A,B))
        [timeElapsed, iterations, result, rel_error] = solve_with(@mldivide, A, MAX_ITERATIONS, MAX_ERROR, MAX_TOLERANCE);
        fprintf(outFile,'%s;%f;%s;%f;Standard;%f;\n',fileName, timeElapsed, result, iterations, rel_error);
        
        fprintf(memFile, 'Matrix:%s;Memory usage:%f;\n',fileName, mem.bytes);
        
    end
    
    fprintf(outFile,'MAX_ITERATIONS:%d;\nMAX_TOLERANCE:%f;\nMAX_ERROR:%f;\n', MAX_ITERATIONS, MAX_TOLERANCE, MAX_ERROR);
        
    fclose('all');
end