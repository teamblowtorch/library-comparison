function [] = solve(File, outFile, maxit, max_err)
    
    A = File.Problem.A;
    
    [length,m] = size(A);
    
    ones_check = ones(length, 1);
    x = ones(length, 1);
    
    b = A*x;
        
    tol = 1e-8; 
    
    tic;
    x = A\b;
    time1 = toc * 1000; % conversione in millisecondi!
    
    if abs(x - ones_check) < max_err
        fprintf(outFile,'%s;%f;Converged;;Standard;\n',File.Problem.name, time1);
    else
        fprintf(outFile,'%s;%f;Diverged;;Standard;\n',File.Problem.name, time1);
    end
    tic;
    [x,flag,relres,iter] = bicgstab(A, b, tol, maxit);
    time2 = toc * 1000; % conversione in millisecondi!
        
    if flag == 0 % Il metodo convergeA
        if abs(x - ones) < max_err % Il risultato � corretto?
            fprintf(outFile,'%s;%f;Converged;%d;Biconjugate gradients;\n',File.Problem.name, time2,iter);
        else
            fprintf(outFile,'%s;%f;Diverged;%d;Biconjugate gradients;\n',File.Problem.name, time2,iter);
        end
    elseif flag == 1 % Numero iterazioni massime raggiunto
        fprintf(outFile,'%s;%f;MaxIterReached;%d;Biconjugate gradients;\n',File.Problem.name, time2,iter);
    else
        fprintf(outFile,'%s;%f;Failed;%d;Biconjugate gradients;\n',File.Problem.name, time2,iter);
    end
    
    tic;
    [x,flag,relres,iter] = bicg(A, b, tol, maxit);
    time3 = toc * 1000; % conversione in millisecondi!
         
    if flag == 0
        fprintf(outFile,'%s;%f;Converge;%d;Biconjugate gradient stabilized;\n',File.Problem.name, time3, iter);
    elseif flag == 1
        fprintf(outFile,'%s;%f;Diverged;%d;Biconjugate gradient stabilized;\n',File.Problem.name, time3, iter);
    else
        fprintf(outFile,'%s;%f;Failed;%d;Biconjugate gradient stabilized;\n',File.Problem.name, time3, iter);
    end
    
end