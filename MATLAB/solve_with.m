function [timeElapsedInMs, iterations, result, relative_error] = solve_with(method_handle, A, max_it, max_err, max_tol)
    [length, m] = size(A);
    
    ones_array = ones(length, 1);
    x = ones(length, 1);
    b = A * x;
    handler_type = functions(method_handle);
    
    tic;
    if(strcmp(handler_type.function, 'mldivide') == 1) % Se si sta usando \ chiama solo con 2 argomenti
        x = method_handle(A, b);
        flag = 0;
        iterations = 0;
    else
        [x, flag, residual, iterations] = method_handle(A, b, max_tol, max_it);
    end
    timeElapsedInMs = toc * 1000;
    
    relative_error = norm(x - ones_array , inf);
    
    if flag == 0 % Il metodo converge (?)
        if abs(x - ones_array) <= max_err % Il risultato � corretto?
            result = 'Converged';
        else
            result = 'Diverged';
        end
    elseif flag == 1 
        result = 'MaxIterReached'; % Numero massimo di iterazioni raggiunte
    else
        result = 'Failed'; % Il metodo ha fallito
    end
end
    