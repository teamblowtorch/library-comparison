% Gauss Seidel method
% INPUT  = A system matrix; b rhs; x0 initial guess; tol tolerance; nmax maximum number of iteration
% OUTPUT = x solution, nit number of iterations, time elapsed time, err final error
function [x,flag ,res, nit]=gauss_seidel(A,b, tol, nmax)

    [M,N]=size(A);
    x0 = zeros(M, 1);
    L=length(x0);
    res = 0;
    flag = 2;
    
    %extract needed matrices
    L = tril(A);
    B = A - L;
    L_inv = L^(-1);

    xold = x0;
    xnew = randi([0,100], M, 1);
    nit=0;
    err = 1;

    while err>tol && nit < nmax
        xold = xnew;
        xnew = L_inv * (b - B*xold);
        nit = nit+1;
        err = norm(xnew-xold, inf);
    end

    if nit >= nmax
        flag = 1;
    else
        flag = 0;
    end

    x   = xnew;

end