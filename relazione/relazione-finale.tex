\documentclass{article}

\usepackage[italian]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{graphicx} % Required for the inclusion of images
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath, amsfonts, amsthm} % Required for some math elements 
\usepackage{hyperref}
\hypersetup{
     colorlinks   = true,
     citecolor    = blue
}
\usepackage{tabu}
\usepackage{float}
\usepackage{booktabs} % for \specialrule command
\usepackage{array}
\newcolumntype{?}{!{\vrule width 1pt}} % riga verticale in grassetto
\usepackage{pdflscape}
\usepackage{lmodern} % Use latin modern font1
\usepackage[a4paper]{geometry} 
\geometry{a4paper,tmargin=3.5cm, bmargin=2.5cm, lmargin=2.75cm, rmargin=2.75cm, headheight=3em, headsep=1.5cm, footskip=2cm}

%\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)
\newcommand{\expnumber}[2]{{#1}\mathrm{e}{#2}} % notazione scientifica
%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Metodi del calcolo scientifico \\ Relazione progetto 1 \\ Algebra lineare numerica - metodi iterativi per matrici sparse } % Title

\author{Darius \textsc{Sas} -- 764303 \\ Simone \textsc{Zini} -- 764800 } % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date


% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	INTRODUZIONE
%----------------------------------------------------------------------------------------

\section{Introduzione}
Con lo scopo di confrontare diverse ambienti di programmazione per la risoluzione di sistemi lineari su matrici sparse mediante metodi \textit{iterativi}, sono stati presi in considerazione i seguenti linguaggi, e le rispettive librerie:
\begin{itemize}
\item \textbf{C\#}: la libreria open-source scelta è \textbf{Math.NET};
\item \textbf{C++}: tra le diverse librerie esistenti open-source si è scelto di testare \textbf{armadillo};
\item \textbf{MATLAB}: gli strumenti di risoluzione integrati nella libreria standard.
\end{itemize}

\subsection{Modello di testing generale}
Le matrici del gruppo \textit{FEMLAB} sono state utilizzate in ciascun linguaggio per risolvere un sistema sistema $Ax=b$ con $b = A\times(1, 1, ..., 1)^T$ (come da testo di esame).

Sono stati generati una serie di test in ciascun linguaggio di programmazione che variano automaticamente una serie di parametri: numero di iterazioni massime e metodo di calcolo utilizzato.
Ogni test eseguito ha generato dei file CSV contenente i dati raccolti durante l'esecuzione del test. In particolare si è scelto di monitorare i seguenti parametri:
\begin{itemize}
	\item velocità di esecuzione, misurata in millisecondi;
	\item numero di iterazioni, quando possibile;
	\item errore relativo;
	\item risultato dell'esecuzione del metodo, ovvero se il metodo converge(\textsc{Converged}), diverge\\(\textsc{Diverged}), raggiunge il numero massimo di iterazioni (\textsc{MaxIterReached}), fallisce (\textsc{Failed}) oppure l'esecuzione viene cancellata in quanto eccessivamente lunga (\textsc{Cancelled}).
\end{itemize}
Tali parametri sono stati utilizzati per la generazione di vari grafici in modo tale da permettere un confronto semplice dei risultati ottenuti dalle varie librerie testate.

% Parametri monitorati

%----------------------------------------------------------------------------------------
%	C# - Math.NET
%----------------------------------------------------------------------------------------

\section{Analisi di Math.NET}
Math.NET è una libreria open-source che implementa diverse funzioni matematiche, tra cui anche diversi metodi per la risoluzione dei sistemi lineari con matrici sparse del tipo $Ax = b$.
% I metodi presenti per la risoluzione
\subsection{Metodi di risoluzione}
I metodi di risoluzione implementati da Math.NET sono i seguenti metodi \textit{iterativi}:
\begin{description}
	\item [GpBiCg] acronimo di Generalized Product Bi-Conjugate Gradient\footnote{Vedi \href{http://dl.acm.org/citation.cfm?id=606712.606720}{\textit{GPBiCG(m,l): A hybrid of BiCGSTAB and GPBiCG methods with efficiency and robustness}}}, è un metodo che può essere utilizzato anche su matrici non simmetriche;
	\item [TFQMR] acronimo di Transpose Free Quasi-Minimal Residual\footnote{Vedi \href{http://www-users.cs.umn.edu/~saad/IterMethBook_2ndEd.pdf}{\textit{Iterative methods for sparse linear systems}} sezione 7.4.3, pag. 247.};
	\item [BiCgStab] acronimo di Bi-Conjugate Gradient Stabilized\footnote{Vedi \href{http://www.netlib.org/linalg/html_templates/Templates.html}{\textit{Templates for the solution of linear systems: Building blocks for iterative methods}}.}, anch'esso può essere utilizzato su matrici non simmetriche;
	\item [MlkBiCgStab] acronimo di Multiple-Lanczos Bi-Conjugate Gradient stabilized, è presentato come un miglioramento di di BiCgStab, ma bisogna scegliere il precoditioner adatto affinchè sia ottimale.
\end{description}

Oltre ai suddetti metodi di risoluzione, Math.NET mette a disposizione anche diversi metodi di trasformazione della matrice $A$ per favorire il calcolo della soluzione, detti preconditioner. 
Tali metodi non sono stati utilizzati nelle analisi effettuate in quanto, non avendo una conoscenza specifica degli algoritmi di risoluzione, non avremmo potuto sfruttarli al meglio.

% Metodologia di testing
\subsection{Metodologia di testing}
Dato che Math.NET offre diverse opzioni di risoluzione, per analizzare in modo approfondito il comportamento di ciascun metodo su ciascuna matrice, si è scelto di effettuare un test combinatorio automatico variando i seguenti parametri:
\begin{itemize}
	\item Metodo di risoluzione: i metodi elencati precedentemente;
	\item Numero massimo di iterazioni: impostato a 1000, 3000, 5000 oppure 10000.
	\item Errore massimo: ovvero un valore $\epsilon \in\mathbb{R}$ tale per cui ciascun elemento della soluzione identificata dal metodo non si può discostare eccessivamente dal valore 1 (ovvero il valore presso cui ciascun elemento dovrebbe convergere).
	I valori presi in considerazione inizialmente sono stati $\epsilon = 0.1$, $\epsilon = 0.08$ e $\epsilon = 0.01$, quest'ultimo però è stato scartato in quanto invalidava troppo facilmente i risultati: in alcuni casi i vettori trovati dal metodo erano di migliaia e migliaia di elementi, quindi, anche se solamente un elemento si discostava eccessivamente da 1, l'intero risultato veniva contrassegnato come errato.
\end{itemize}
Inoltre, si è scelto, euristicamente, di impostare un tempo massimo di esecuzione pari a 4 minuti in quanto durante la fase di testing dell'implementazione ci si è accorti che alcuni metodi entravano in uno stato di loop e impedivano l'esecuzione dei test successivi.

% Confronto dei metodi
% Fare grafici dei tempi

%----------------------------------------------------------------------------------------
%	C++ - Armadillo
%----------------------------------------------------------------------------------------


\section{Analisi di Armadillo C++ linear algebra library}

Per C++ è stata scelta la libreria Armadillo - C++ linear algebra library; 
Armadillo offre una sintassi molto simile a quella di MATLAB, e mette a 
disposizione classi per la rappresentazione di vettori, matrici e cubi,
con associati più di 200 funzioni ciascuno per potervi lavorare.
La libreria è integrata inoltre con LAPACK - Linear Algebra PACKage -, una 
libreria Fortran 90 che fornisce metodi di decomposizione delle matrici, con lo
scopo di migliorare le prestazioni per la risoluzione di sistemi lineari con
matrici di grosse dimensioni (ad esempio sfruttando tecniche di multi-threading).

Le classi che sono state effettivamente utilizzate sono:
\begin{itemize}
\item Mat<type>: Matrice densa; questa classe permette di creare matrici dense con 
			oggetti di tipo type (nel nostro caso specifico di tipo double).

\item SpMat<type>: Matrice sparsa; classe per la rappresentazione di matrici sparse
			  con elementi di tipo type.

\item Col<type>: Vettore colonna; classe per la rappresentazione di vettori colonna 
			con elementi di tipo type.
\end {itemize}
Armadillo fornisce una funzione specifica per la risoluzione di sistemi lineari
con matrici sparse. La funzione prende il nome di \verb-spsolve()- e presenta differenti
opzioni:
\begin{verbatim}
X = spsolve( A, B ) 
X = spsolve( A, B, solver ) 
X = spsolve( A, B, solver, settings ) 
\end{verbatim}
Nelle modalità sopraelencate la funzione solve, date in input la matrice A e il 
vettore B, calcola la soluzione X, se possibile.
Se non ci sono soluzioni la funzione lancia un'eccezione a runtime.
\begin{verbatim}
spsolve( X, A, B ) 
spsolve( X, A, B, solver ) 
spsolve( X, A, B, solver, settings )
\end{verbatim}
Passando invece X come parametro, oltre alle matrici A e B, la funzione verifica
la correttezza di X, dando come valore di ritorno un booleano \verb-true- nel caso
positivo, \verb-false- altrimenti (nel caso il sistema non dovesse avere soluzioni, viene
ritornato il valore \verb-false-).	

Vi sono due parametri opzionali: \verb-solver- e \verb-settings-.

Il parametro \verb-solver- permetti di selezionare quale algoritmo di risoluzione
utilizzare.
Ve ne sono due a disposizione:
\begin{itemize}
\item "superlu" : risolutore di default, tramite il parametro opzionale settings
			  è possibile settare alcuni valori per variare la risoluzione.

\item "lapack" : la funzione sfrutta i solver della libreria LAPACK; per poter 
			 essere elaborata successivamente, la matrice sparsa viene convertita
			 in una matrice densa, con una conseguente occupazione di memoria
			 maggiore.
\end{itemize}
Il paramentro opzionale \verb-settings- è strettamente legato al \verb-solver- "superlu" ed è
una collezione di parametri per impostare l'algoritmo di risoluzione a proprio
piacimento.

Il parametro è un istanza della \verb-struct superlu_opt- definita come segue:
\begin{verbatim}
struct superlu_opts
  {
  bool             equilibrate;  // default: false
  bool             symmetric;    // default: false
  double           pivot_thresh; // default: 1.0
  permutation_type permutation;  // default: superlu_opts::COLAMD
  refine_type      refine;       // default: superlu_opts::REF_DOUBLE
  };
\end{verbatim}
Informazioni più dettagliate sul resto della libreria sono presenti nella 
\href{http://arma.sourceforge.net/docs.html}{documentazione di Armadillo}.

%----------------------------------------------------------------------------------------
%	MATLAB
%----------------------------------------------------------------------------------------


\section{Matlab}

Per quanto riguarda l'ambiente Matlab si è scelto di testare alcuni tra i
metodi iterativi messi a disposizione dall'ambiente. In Matlab sono presenti
undici funzioni adibite alla risoluzione di sistemi lineari tramite iterazione:
\begin{center}

\begin{tabular}{ |c|c| }
\hline
\textbf{Function} & \textbf{Method} \\
\hline
bicg & Biconjugate gradient \\
\hline
bicgstab & Biconjugate gradient stabilized \\
\hline
bicgstabl & Biconjugate gradient stabilized (l) \\
\hline
cgs & Conjugate gradient squared \\
\hline
gmres & Generalized minimum residual \\
\hline
lsqr & Least squares \\
\hline
minres & Minimum residual \\
\hline
pcg & Preconditioned conjugate gradient \\
\hline
qmr & Quasiminimal residual \\
\hline
symmlq & Symmetric LQ \\
\hline
tfqmr & Transpose-free quasiminimal residual\\
\hline

\end{tabular}

\end{center}

Di queste undici funzioni si è scelto di testare, oltre al risolutore standard (\verb-x = A \ b-), i metodi \textit{bicg}, \textit{bigstab} e \textit{Jacobi} e \textit{Gauss-Seidel}. 
Questi metodi, oltre a ritornare come valore di output il risultato \verb-x-, forniscono una serie di informazioni sui calcoli effettuati.\\
Consideriamo \textit{bicg} come esempio (per \textit{bicgstab} i valori di output e input sono analoghi):
\begin{verbatim}

[x,flag,relres,iter] = bicg(A,b,tol,maxit)

\end{verbatim}

\textbf{Parametri in input:}
\begin{itemize}

\item [\textbf{tol}] specifica la tolleranza del metodo. il valore di default è impostato a 1e-6.

\item [\textbf{maxit}] specifica il numero massimo di iterazioni. Il valore di default è 20.

\end{itemize}

\textbf{Valori in output:}

\begin{itemize}

\item [\textbf{flag}] flag utilizzata per indicare informazioni sulla convergenza del metodo. Ha diversi valori possibili:

\begin{center}
\begin{tabular}{|m{1cm}|m{8cm}|}

\hline

\textbf{Flag} & \textbf{Convergenza} \\
\hline
0 & \textit{bicg} converce con la tolleranza desiderata ed entro il numero di iterazioni massimo.\\
\hline
1 & bicg ha iterato fino al massimo numero di iterazioni possibili, senza convergere.\\
\hline
2 & Il preconditioner è stato \textit{'condizionato'} in modo errato.\\
\hline
3 & Due iterazioni continue hanno restituito lo stesso risultato.\\
\hline
4 & Errore interno. \\
\hline

\end{tabular}
\end{center}
\item[\textbf{relres}] ritorna il residuo \verb+norm(b-A*x)/norm(b)+. Se flag vale 0, $relres <= tol$.
\item[\textbf{iter}] ritorna il numero di iterazioni che sono state fatte.
\end{itemize}

%----------------------------------------------------------------------------------------
%	CONFRONTO
%----------------------------------------------------------------------------------------

\section{Confronto tra i linguaggi e le rispettive librerie}
\subsection{Dimensioni in memoria delle matrici}
\begin{table}
\caption{Dimensione in memoria (in MegaByte) delle matrici.}
\label{tab:tab_memoria}
\begin{center}
\begin{tabular}{|r|c|c|c|c|c|}\hline
\textbf{Matrice} & \textbf{Righe} & \textbf{Colonne} & \textbf{Non-zero} & \textbf{MB in Matlab} & \textbf{MB in C\#} \\ \hline
ns3Da &	20414	& 20414 &	1679599 & 25,78 & 19,31 \\\hline
poisson2D	& 367 & 367 &	2417 & 0,04 & 0,03 \\\hline
poisson3Da & 13514 & 13514 & 352762 & 5,49 & 4,09 \\\hline
poisson3Db &	85623 &	85623 &	2374949 & 36,89 & 27,51 \\\hline
problem1	& 415 &	415 &	2779 & 0,05 & 0,04 \\\hline
sme3Da &	12504 &	12504 &	874887 & 13,45 & 10,06 \\\hline
sme3Db &	29067 &	29067 &	2081063 & 31,98 & 23,93 \\\hline
sme3Dc &	42930 &	42930 &	3148656 & 48,37 & 36,20 \\\hline
\end{tabular}
\end{center}
\end{table}
La tabella \ref{tab:tab_memoria} illustra la dimensione in memoria delle tabelle del gruppo \textit{FEMLAB} in ciascun linguaggio di programmazione.
Purtroppo non è stato possibile determinare la dimensione in memoria delle matrici in C++.

\subsection{Risultati Math.NET C\#}
\paragraph{Tempi e iterazioni}
Il grafico in figura \ref{fig:csharp_methods_time_image} confronta i tempi di esecuzione di ciascun metodo testato in Math.NET C\#.
Come evidenzia il grafico il metodo che in generale si è comportato meglio è GpBiCg, il quale è riuscito a garantire una buona stabilità e i migliori tempi di esecuzione, eccetto per la matrice poisson3Db, sulla quale BiCgStab ha eseguito i calcoli con tempi leggermente migliori.
Il peggior metodo, invece, si è dimostrato essere MlkBiCgStab, il quale non è riuscito a garantire delle prestazioni decenti in quasi nessun caso. La causa di questo risultato è dovuta al fatto che questo metodo dipende molto dal preconditioner utilizzato (come detto precedentemente, non è stato utilizzato alcun preconditioner).

\begin{figure}

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/tempo_iterazioni/csharp_confronto_metodi_mathnet.png}}
	\caption{Confronto dei tempi di esecuzione dei metodi di Math.NET.}\label{fig:csharp_methods_time_image}

\end{figure}

Da notare come nessun metodo è riuscito a convergere sulle matrici problem1, sme3Da, sme3Db, sme3Dc entro il numero massimo di iterazioni (10000).

Dato che non è stato possibile ricavare il numero di iterazioni effettuate, in quanto la libreria non forniva tale informazione a seguito dell'esecuzione, si è deciso di impostare diverse soglie massime del numero di iterazioni. 
In figura \ref{fig:csharp_iteration_time_image} sono illustrati i tempi di esecuzione di GpBiCg al variare del numero di iterazioni massime, inoltre la figura permette di farsi un'idea del numero di iterazioni effettuate dal metodo.
Sulle matrici sulle quali il metodo converge ci sono delle piccole variazioni temporali dovute allo scheduler dei processi del sistema. 
In sintesi si può dire che le matrici che convergono convergono sempre entro le 1000 iterazioni.
\begin{figure}

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/tempo_iterazioni/csharp_variazione_max_iterations_gpbicg.png}}
	\caption{Confronto dei tempi di esecuzione al variare del numero massimo di iterazioni.}\label{fig:csharp_iteration_time_image}

\end{figure}
\paragraph{Errore relativo}
Generalmente, Math.NET garantisce un errore relativo nell'ordine di $\expnumber{1}{-5}$ in caso di convergenza del metodo.
Un fatto interessante è che tale errore cresce all'aumentare della dimensione della matrice.

Dal punto di vista dell'errore relativo il metodo più preciso è TFQMR.
Mentre, dal grafico si nota che MlkBiCgStab termina l'esecuzione con un errore relativo molto alto, segno che comunque non si stava avvicinando alla soluzione del sistema pur avendo tempi di esecuzione molto alti.
I risultati sono evidenziati in figura \ref{fig:csharp_methods_error_image}.

\begin{figure}
	
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/errore_relativo/csharp_confronto_metodi_mathnet.png}}
	\caption{L'errore relativo dei metodi di Math.NET.}\label{fig:csharp_methods_error_image}

\end{figure}

L'errore relativo del GpBiCg, come mostrato in figura \ref{fig:csharp_iterations_error_image}, non varia all'aumentare del numero massimo di iterazioni. Ciò vuol dire che il metodo non cerca una soluzione migliore quando ha a dispozione un numero di iterazioni massime maggiore.

\begin{figure}[H]

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/errore_relativo/csharp_errore_relativo_gpbicg.png}}
	\caption{Variazione dell'errore relativo al variare del numero massimo di iterazioni.}\label{fig:csharp_iterations_error_image}

\end{figure}	

\subsection{Risultati Armadillo - C++}

Nella tabella \ref{tab:tab_risultati_cpp} sono presenti i dati raccolti dalla risoluzione delle varie matrici con l'utilizzo di armadillo, la libreria di C++ presa in considerazione.

Purtroppo i metodi della libreria non forniscono lo stesso numero di informazioni inerenti l'esecuzione rispetto agli altri due ambienti sotto test. Infatti, non è stato possibile né impostare un numero massimo di iterazioni né conoscere il numero di iterazioni eseguite dal metodo.

\begin{table}[H]
\caption{Errore relativo e tempi di esecuzione (in ms) dei metodi di C++.}
\label{tab:tab_risultati_cpp}
\begin{center}
\begin{tabular}{|r|c|c|c|}\hline
\textbf{Nome Matrice} &  \textbf{Millisecondi} & \textbf{Err. relativo} & \textbf{Risultato}\\\hline
ns3Da & 55574 & 1,06E-10 & Converged \\ \hline
poisson2D & 3 & 3,03E-11 & Converged \\ \hline
poisson3Da & 22605 & 1,49E-10 & Converged \\ \hline
poisson3Db & 0 & 0,00E+00 & Failed \\ \hline
problem1 & 0 & 0,00E+00 & Failed \\ \hline
sme3Da & 3842 & 3,07E-07 & Converged \\ \hline
sme3Db & 32299 & 5,83E-07 & Converged \\ \hline
sme3Dc & 43175 & 1,09E-07 & Converged \\ \hline
\end{tabular}
\end{center}
\end{table}

\paragraph{Tempo di esecuzione ed errore relativo}
I tempi di esecuzione sono stati relativamente bassi, soprattutto su matrici di grandi dimensioni.

\paragraph{Errore relativo}
Nella figura \ref{fig:matlab_cpp_error_image} vi è un confronto dell'errore relativo calcolato con Armadillo confrontato con quello risultante da Matlab; si osserva che l'errore calcolato in C++ risulta essere molto più piccolo rispetto a quello di Matlab (ove i metodi convergono), indice quindi della presenza di condizioni molto più strette per la conclusione del ciclo di iterazioni.


\begin{figure}

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/errore_relativo/matlab&cpp_errore_relativo.png}}
	\caption{Confronto dell'errore relativo di Matlab e C++.}\label{fig:matlab_cpp_error_image}

\end{figure}

Rimandiamo altri confronti alla sezione di confronto generale.


\subsection{Matlab}

\paragraph{Tempo di esecuzione}
Consideriamo inizialmente i metodi della libreria standard di Matlab (Standard \verb-\-, BiCg e BiCgStab) nella figura \ref{fig:matlab_methods_time_image}(grafico in scala logaritmica).
Si nota sin da subito che convergono con ogni matrice, tra cui anche le matrici sme3D.
L'unica eccezione è data da problem1 e dal sistema associato ad essa che non è stato possibile risolvere con nessun metodo tra quelli selezionati.
\'E interessante notare le sostanziali differenze sui tempi di esecuzione del metodo di risoluzione standard, il quale è un metodo diretto, con i tempi dei metodi iterativi BiCg e BiCgStab: sulle matrici di dimensioni ridotte il risolutore standard impiega circa un quarto del tempo impiegato dai metodi iterativi, mentre su matrici di dimensioni più sostanziali i metodi iterativi portano a termine l'esecuzione in tempi esponenzialmente più grandi.

Osserviamo ora i dati riguardanti l'esecuzione dei metodi Jacobi e Gauss-Seidel, visti a lezione.
Ricordiamo innanzitutto che gli algoritmi sono quelli scritti in laboratorio e  pertanto privi di ogni tipo di ottimizzazione.
Si nota subito che entrambi i metodi impiegano molto più tempo a terminare l'esecuzione a differenza degli algoritmi della libreria standard; detto questo concentriamo l'attenzione sui due metodi.
Jacobi diverge in tutti i casi a eccezione della matrice poisson2D, mentre Gauss-Seidel richiede maggior tempo ma converge in un numero di casi maggiore; è da notare che con la matrice sme3Dc il metodo di Gauss-Seidel non è giunto al termine, motivo per il quale non compare nel grafico in corrispondenza della matrice.

\begin{figure}

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/tempo_iterazioni/matlab_confronto_metodi.png}}
	\caption{Tempi di esecuzione di Matlab confrontati con Jacobi e Gauss-Seidel. (Scala logaritmica). }\label{fig:matlab_methods_time_image}

\end{figure}

\paragraph{Iterazioni}
Per quanto riguarda il numero di iterazioni necessarie per la risoluzione dei sistemi, notiamo che i metodi di libreria, sulle matrici di modeste dimensioni, convergono sempre in meno di 1000 iterazioni; sulle matrici più grandi il numero di iterazioni rimane sotto la soglia di 7000 (figura \ref{fig:matlab_methods_iteration_image}).
Per quanto riguarda i nostri algoritmi invece, Gauss-Seidel esegue quasi sempre un numero di iterazioni maggiori rispetto a Jacobi, ma con una percentuale di riuscita maggiore. 

\begin{figure}[H]

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/tempo_iterazioni/matlab_iterazioni_metodi.png}}
	\caption{Confronto delle iterazioni effettuate dai metodi di Matlab.}\label{fig:matlab_methods_iteration_image}

\end{figure}



%----------------------------------------------------------------------------------------
%	RISULTATI
%----------------------------------------------------------------------------------------

\section{Considerazioni generali e conclusioni}

Nella figura \ref{fig:confronto_image} (scala logaritmica) vi è un confronto tra i diversi ambienti considerati, con le matrici su cui sono stati effettuati i calcoli e i relativi tempi di esecuzione.

\begin{figure}

	\centering
	\centerline{\includegraphics[width=1.15\linewidth]{grafici/tempo_iterazioni/confronto_generale_librerie.png}}
	\caption{Confronto generale dei tempi di esecuzione dei tre ambienti sotto test.}\label{fig:confronto_image}

\end{figure}

Come si può osservare, Matlab è l'ambiente più performante; porta a termine i calcoli con ogni matrice, eccetto per problem1 che risulta essere un caso critico irrisolvibile in ciascun ambiente di sviluppo.
Osservando i dati possiamo azzardare l'ipotesi che gli algoritmi sfruttino la tecnologia multithread in maniera ottimizzata, in quanto in ogni situazione i tempi di calcolo sono drasticamente più bassi.
Concentriamo l'attezione su C++ e C\#: sulle matrici più piccole Math.NET risulta essere più performante di Armadillo, tuttavia al crescere delle dimensioni, Armadillo riesce a risolvere ogni sistema, mentre Math.NET fallisce.
Tuttavia la libreria di C++ offre delle interfacce molto più povere rispetto alla libreria di C\#; non è infatti possibile ricavare informazioni sull'esecuzione, come il numero di iterazioni o altre statistiche. Anche i parametri settabili sono relativamente pochi in confronto alle funzioni presenti in Math.NET.

In conclusione Math.NET C\# presenta delle ottime interfacce, con un gran numero di parametri impostabili e di informazioni reperibili sull'esecuzione degli algoritmi, ma risulta più debole dal punto di vista delle prestazioni, probabilmente per via della necessità dell'uso di thread per matrici di grosse dimensioni e dell'uso di preconditioner (da noi non considerati).
Armadillo presenta invece delle interfacce semplici, con un limitato numero di impostazioni e dati telemetrici, tuttavia riesce a effettuare calcoli in tempi relativamente brevi anche sui sistemi più complessi, dimostrandosi un ambiente facile da utilizzare e allo stesso tempo molto potente.
Infine Matlab risulta essere, come ci si aspettava, l'ambiente più performante e più elaborato tra tutti; le alte performance sono probabilmente dovute, come supposto in precedenza, da un ottimo utilizzo del multithreading e del corretto uso dei preconditioner per le matrici, anche per le più grandi.

\end{document}
% ERRORE RELATIVO -----------------------------------------------------------------------



% TEMPO-ITERAZIONI ---------------------------------------------------------------------
