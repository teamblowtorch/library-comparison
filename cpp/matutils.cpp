#include "matutils.h"


void print_files( vector<string> file_list ){

	cout << "========================" << " File list " << "========================" << endl;
	for (vector<string>::const_iterator i = file_list.begin(); i != file_list.end(); ++i) {
		cout << *i << endl;
	}
}


SpMat<double>* file_parse(string file_path) {

	int M, N, L;
	
	cout << endl << "======================" << " File Parsing " << "=======================" << endl;

	std::ifstream fin(file_path);

	while(fin.peek() == '%') fin.ignore(2048,'\n');
	fin >> M >> N >> L;

	cout << endl << "===" << "File: " << file_path << endl;
	cout << "M: "<< M << "\nN: "<< N << "\nL: "<< L << endl;
	
	
	int m, n;
	double data;

	Mat<double>* bvec = new Mat<double>(M,1);
	Col<double>* xevec = new Col<double>(M);
	
	xevec->fill(1.0);
	
	SpMat<double>* spmat = new SpMat<double>(M,N); //<< sparse matrix finale
	
	cout << "Parsing file:" << endl;

	for(int i=0; i<L;i++){
		cout << i << "\r";
		fin >> m >> n >> data;
		spmat->at(m-1,n-1) = data;
	}
	
	cout << "Completed \r";

	cout << endl;
	fin.close();

	return spmat;
}


std::vector <std::string> load_from_dir(const std::string& path = std::string()) {
	
	std::vector <std::string> file_list;	//<< lisa di file generale
	std::vector <std::string> result;		//<< lista di file selezionati (solo .mtx)

	dirent* de;
	DIR* dp;
	errno = 0;
	string working_dir = "";

	if(path.empty()) {
		dp = opendir( "." );
		working_dir = ".";
	}
	else{
		dp = opendir( path.c_str() );
		working_dir = path.c_str();
	}
	
	if (dp) {
		while (true) {
		  	errno = 0;
		  	de = readdir( dp );
		  	if (de == NULL) break;
		  	file_list.push_back( std::string( de->d_name ) );
		}
		closedir( dp );
		std::sort( file_list.begin(), file_list.end() );
	}
	

	// Selezione dei file con estensione .mtx nella directory

	string fn;
	for (vector<string>::const_iterator i = file_list.begin(); i != file_list.end(); ++i) {
		
		if(i->substr(i->find_last_of(".") + 1) == "mtx") {
			result.push_back(working_dir + "/" + *i);
		}
	}

	if(result.empty()) {
		throw file_not_found();
	}
	else {
		return result;
	}
}


std::vector<SpMat<double>*> parse_file_list( vector<string> file_list) {

	vector<SpMat<double>*> result;

	for (vector<string>::const_iterator i = file_list.begin(); i != file_list.end(); ++i) {

		result.push_back(file_parse(*i));

	}

	return result;
}

stats matrix_list_solver(vector<string> file_list) {

	SpMat<double>* spmat;

	stats telemetry;

	for (vector<string>::const_iterator i = file_list.begin(); i != file_list.end(); ++i) {

		spmat = file_parse(*i);

		telemetry.matrix_name = *i;

		cout << endl << "=====================" << " Solving started " << "=====================" << endl;	

		matrix_solver(spmat, telemetry);

		cout << endl << "======================" << " Solving ended " << "======================" << endl;
	}
}

void matrix_solver(SpMat<double>* spmat, stats& telemetry) {

	Mat<double> b;
	Mat<double> x;
	Mat<double> solution;
	bool status = false;
	timeval start, end;
	double elapsed_time;

	telemetry.cols = spmat->n_cols;
	telemetry.rows = spmat->n_rows;
	telemetry.nonzero = spmat->n_nonzero;

	
	x = x.ones(telemetry.cols);	
	
	b = *spmat * x;

	gettimeofday(&start, NULL);

	try {
		solution = spsolve(*spmat, b);

	}
	catch (std::runtime_error& e) {
		telemetry.solved = false;
		status = true;
	}

	gettimeofday(&end, NULL);

	elapsed_time = (end.tv_sec - start.tv_sec) * 1000.0; 		// sec to ms
	elapsed_time += (end.tv_usec - start.tv_usec) / 1000.0;		// us to ms

	telemetry.time_elapsed = elapsed_time;

	if(!status){
		cout << "=== Solution found ===" << endl;
		telemetry.solved = true;
		telemetry.err = norm(solution - x, 2)/norm(x, 2);
		cout << "Execution time: " << telemetry.time_elapsed << " ms" << endl;
		telemetry.save_stats();
	}
	
}