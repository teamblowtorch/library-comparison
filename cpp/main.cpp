#include <iostream>
#include <armadillo>

#include "matutils.h"

using namespace std;
using namespace arma; // armadillo

int main(int argc, char**argv) {

	vector<string> file_list = load_from_dir(argv[1]);

	print_files(file_list);

	matrix_list_solver(file_list);
}