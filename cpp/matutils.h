#ifndef MATUTILS_H

#define MATUTILS_H

	#include <stdexcept>
	#include <algorithm>
	#include <string>
	#include <vector>
	#include <iostream>
	#include <fstream>
	#include <armadillo>
	#include <string>
	#include <dirent.h>
	#include <sys/types.h>
	#include <sys/time.h>

	using namespace std;
	using namespace arma; //armadillo

	struct stats{
		string matrix_name;
		int rows, cols, nonzero;
		bool solved;
		double time_elapsed; 	// in milliseconds
		int memory_used;  		// in MBytes (se possibile)
		int iterations;  		// iterazioni (se possibile)
		double err; 			// errore relativo

		void save_stats(){

			bool exist = false;

			ifstream file("telemetry_results.csv");
			if(file){
				exist = true;
			}
			file.close();

			ofstream myfile;
  			
  			myfile.open("telemetry_results.csv", ios::app);
  			
  			if(!exist){
  				myfile << "Nome Matrice; Righe; Colonne; Valori non zero; Stato risoluzione; Tempo di risoluzione(ms); Errore relativo;" << endl;
  			};
  			myfile << matrix_name <<";"<< rows <<";"<< cols <<";"<< nonzero <<";"<< (solved ? "risolto" : " non risolto") <<";"<< time_elapsed << ";" << err << endl;
  			myfile.close();

		};
	};

	/**
	 * Eccezione per file non trovati
	 * @brief Eccezione per file non trovati
	 */
	class file_not_found : public std::runtime_error {
	public:
		file_not_found() : runtime_error("Non ci sono file di tipo .mtx nella cartella") {}
	};

	/**
	*	Funzione per stampare a console una lista path/file
	*	@brief Funzione per stampare a console una lista path/file
	*	@param file_list vettore template string da stampare a video
	*/	
	void print_files( vector<string> file_list );

	/**
	*	Funzione per estrarre una matrice sparsa dal file dato in input
	*	@brief Funzione per estrarre una matrice sparsa dal file dato in input
	*	@param file_path path del file da parsare
	*	@return matrice sparsa ottenuta dal parsing del file
	*/	
	SpMat<double>* file_parse(string file_path);

	/**
	*	Funzione per caricare una serie di matrici da una directory contenente file .mtx
	*	@brief Funzione per caricare una serie di matrici da una directory contenente file .mtx
	*	@param path path della directory in cui sono presenti i file con estensione .mtx
	*	@return vettore contente i path con i file .mtx 
	*/
	std::vector <std::string> load_from_dir(const std::string& path);

	/**
	*	Funzione per parsare una serie di matrici a partire da una lista path/file
	*	@brief Funzione per parsare una serie di matrici a partire da una lista path/file
	*	@param file_list lista di path/file da parsare
	*	@return vettore contenente le matrici sparse estratte dai file .mtx 
	*/
	std::vector<SpMat<double>*> parse_file_list( vector<string> file_list);


	/**
	*	Funzione per eseguire la risoluzioni di sistemi lineari con matrici sparse
	*	@brief Funzione per eseguire la risoluzioni di sistemi lineari con matrici sparse
	*	@param file_list lista di file contenenti le matrici sparse
	*/
	stats matrix_list_solver(vector<string> file_list);

	/**
	*	Funzione per calcolare la soluzione di sistemi lineari con matrice sparsa in input
	*	@brief Funzione per calcolare la soluzione di sistemi lineari con matrice sparsa in input
	*	@param spmat matrice sparsa su cui effettuare il calcolo 
	*	@param stats struttura dati di tipo stats contente informazioni sull'esecuzione
	*/
	void matrix_solver(SpMat<double>* spmat, stats& telemetry);
;

#endif